# -*- coding: utf-8 -*-

import plotly.graph_objects as go

fig = go.Figure()
fig.add_trace(go.Scattergl(x=[1, 2, 3], y=[4, 5, 6]))

from plotneat.render import render

render(fig, "test.html")
render(fig, "test.json")
render(fig, "Image")
render(fig, "test.png")
render(fig, "test.jpeg")
render(fig, "test.pdf")

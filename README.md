# plotneat

This package is meant help with simple neat plotting in Python 3.

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

## Capabilities

None yet

## Installation

### Development versions
If you want to use the development version, then just keep on using the one in the [repository on Gitlab](https://gitlab.com/Pierre_VF/plotneat) by checking out the right branches:

To get the latest stable version, pull the master branch from Gitlab:
~~~ 
pip install git+https://gitlab.com/Pierre_VF/plotneat.git@master
~~~ 

To get the latest developments (can be unstable), pull the Development branch from Gitlab:
~~~ 
pip install git+https://gitlab.com/Pierre_VF/plotneat.git@Development
~~~ 
